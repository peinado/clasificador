#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    correcto =False

    if string.find ("@") != -1 and string.find ("@") != 1 and string.rfind (".") != -1 and string.rfind (".") != string.find ("@")+1:
        correcto = True

    return correcto

def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


def es_real(entrada):
    try:
        float(entrada)
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    devolver = ""
    if string == "":
        devolver = None
    elif es_correo_electronico(string):
        devolver = "Es un correo electrónico."
    elif es_entero(string):
        devolver = "Es un entero."
    elif es_real(string):
        devolver = "Es un número real."
    else:
        devolver = "No es ni un correo, ni un entero, ni un número real."

    return devolver

def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
